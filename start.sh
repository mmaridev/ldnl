#!/bin/bash

if ! [ -e .env ] ; then
	./genpw.sh
fi

docker-compose up -d
docker exec code bash -c "echo $(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' nextcloud) nc.local >> /etc/hosts"

echo $(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' code) is code.local
