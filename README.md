# LDNL: Linux+Docker+Nextcloud+LibreOffice Online

This is the repository used for
[this](https://www.lugbz.org/events/ldnl-linux-docker-nextcloud-libreoffice-online/) 
workshop @ LugBZ.


## Quickstart

1. Install docker and docker-compose
2. Clone this repository
3. Run ./genpw.sh to generate random passwords (make sure pwgen is installed in your system)
3. Run `docker-compose up -d`
4. Set the DNS up (and eventually your proxy)
5. Enjoy
